defmodule Channeler.Socket do
  use Phoenix.Socket

  channel "events:lobby", Channeler.Channel

end
