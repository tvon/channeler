defmodule Channeler.ChannelTest do
  use ExUnit.Case
  use Phoenix.ChannelTest

  @endpoint Channeler.Endpoint

  setup do
    {:ok, _, socket} =
      socket("user_id", %{some: :assign})
      |> subscribe_and_join(Channeler.Channel, "events:all")

    {:ok, socket: socket}
  end

  test "push replies with status ok", %{socket: socket} do
    ref = push socket, "push", %{"hello" => "there"}
    assert_reply ref, :ok, %{"hello" => "there"}
  end

  test "broadcast broadcasts to events:all", %{socket: socket} do
    push socket,     "broadcast", %{"hello" => "all"}
    assert_broadcast "broadcast", %{"hello" => "all"}
  end

  test "broadcasts are pushed to the client", %{socket: socket} do
    broadcast_from! socket, "broadcast", %{"some" => "data"}
    assert_push "broadcast", %{"some" => "data"}
  end
end

