defmodule Channeler.Channel do
  use Phoenix.Channel

  def join("events:" <> id, payload, socket) do
    if authorized?(payload) do
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  def join(_channel_identifier, payload, socket) do
    {:ok, socket}
  end

  def handle_in("push", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  def handle_in("broadcast", payload, socket) do
    broadcast socket, "broadcast", payload
    {:noreply, socket}
  end

  def handle_out(event, payload, socket) do
    push socket, event, payload
    {:noreply, socket}
  end

  defp authorized?(payload) do
    true
  end

end
