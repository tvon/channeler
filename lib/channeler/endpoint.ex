defmodule Channeler.Endpoint do
  use Phoenix.Endpoint, otp_app: :channeler

  socket "/socket", Channeler.Socket

  #plug Plug.RequestId
  #plug Plug.Logger

  #plug Plug.Parsers,
  #  parsers: [:urlencoded, :multipart, :json],
  #  pass: ["*/*"],
  #  json_decoder: Poison

  #plug Plug.MethodOverride
  #plug Plug.Head

  #plug Plug.Session,
  #  store: :cookie,
  #  key: "_breadcrumbs_key",
  #  signing_salt: "O/itclx9"

  #plug Breadcrumbs.Router
end
