use Mix.Config

config :channeler, Channeler.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "admin",
  password: "admin",
  database: "channeler_dev",
  hostname: "localhost",
  pool_size: 10

