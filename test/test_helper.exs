ExUnit.start()

Mix.Task.run "ecto.create", ~w(-r Channeler.Repo --quiet)
Mix.Task.run "ecto.migrate", ~w(-r Channeler.Repo --quiet)
Ecto.Adapters.SQL.begin_test_transaction(Channeler.Repo)
