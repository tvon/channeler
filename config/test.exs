use Mix.Config

config :channeler, Channeler.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "admin",
  password: "admin",
  database: "channeler_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
